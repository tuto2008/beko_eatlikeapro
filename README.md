# README #

Paso a seguir para implementar la web en el desarrollo del cliente

### Como instalar ###

Los archivos que hay que integrar son los que tienen extensión .html (home.html, mission.html, recipes.html, smoothies.html). Se cargan todos dentro del index.php para poder agregar elementos comunes a todos y que fuera más fácil el desarrollo.
Todo el contenido debe estar sobre un contenedor al 100% de ancho en la plantilla, ya que hay contenidos que ocupan todo lo ancho y no quedarán bien si se integran en un contenedor con max-width.
En el index.php hay estilos en linea para resetear todos los elementos del html, habrá que eliminarlos al integrar en la página final.
Los estilos estan todos aplicados a partir de la clase .LPELP_mainSection.

### Compartir en Facebook y Twitter ###

Para los links de compartir habrá que crear los meta correspondientes para cada página de recetas. Ver index.php en el header para ver un ejemplo con la página de recetas actual.
Los links de share en la página de recetas llevan metas específicos para el texto, la url y el tipo de link, que deben ser configurados para cada caso.

### Correcciones de CSS ###

Si al momento de integrar en la web hay partes de la maqueta que no queda bien integrada, Por favor no eliminar este meta del header de las páginas <link rel="stylesheet" href="http://beko.ondevelope.com/assets/css/fixed.css">, ya que allí es donde iré solucionando los errores de css para luego indicarles los cambios que tenéis que hacer para que quede integrado en la hoja de estilos.
Este css actualmente esta vacío.