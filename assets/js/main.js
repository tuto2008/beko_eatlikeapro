$o 			= {}
$sm 		= {}
$(window).load(function() {
	ajustar();
});

$(function(){
	ajustar();
	$(window).resize(ajustar);
	$(window).bind('orientationchange',ajustar);
	$sm.createSmoothiesCarrusel();
	$('a.LPELP_modalClose').click(closeModal);
	$('a.playVideo').click(openModal);
	$('a.shareRRSS').click(shareThis);
	$('a.nofollow').click(nofollowLink);
});

function ajustar(){
	$margin 	= $('.LPELP_header').outerHeight();
	$w 			= $(window).width();
	$h 			= $(window).height();
	$('.ancho').html($w);
	$alto 		= $h - $margin;
	if($w/$alto > 2.8){
		$alto = $w*0.55;
	}
	if($alto > 1400){$alto = 1400}
	$('.LPELP_mainSection .LPELP_video').height($alto);
	$o.createCarrusel();
}

function nofollowLink(){
	return false;
}

function shareThis(e){
	e.preventDefault();
	type 	= $(this).attr('data-type');
	if(type == 'twitter'){
		url 	= 'http://www.twitter.com/intent/tweet?url=' + $(this).attr('data-url') + "&text=" + $(this).attr('data-text');
		rrss 	= 'Twitter';
	} else {
		url 	= 'https://www.facebook.com/sharer/sharer.php?u=' + $(this).attr('data-text');
		rrss 	= 'Facebook';
	}
	
	config 	= "height=285,width=550,resizable=1";
	window.open(url, "Share on" + rrss, config);
	return false;
}

function openModal(){
	if( !$(this).hasClass('noPlay') ){
		id = $(this).attr('data-playerId');
		$('#LPELP_videoModal').fadeIn(400,function(){
			html = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+id+'?autoplay=1&controls=0&disablekb=1&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>';
			$('.LPELP_videoPlayer').html(html);
		});
	}
	return false;
}

function closeModal() {
	$('#LPELP_videoModal').fadeOut(400,function(){
		$('.LPELP_videoPlayer').html('');
	});
	return false;
}

/* SMOOTHIES NAVIGATION */
	$sm.createSmoothiesCarrusel = function(){
		if($('#LPELP_smoothiesPage').length){
			$sm.obj 	= $('#LPELP_smoothiesPage');
			$sm.total 	= $('.LPELP_smoothiesList li.item').length;
			$sm.list 	= $sm.obj.find('ul');
			$sm.arrows 	= $sm.obj.find('nav.steps');
			$sm.steps 	= $sm.obj.find('nav.pagination');
			$sm.posi    = 0;
			for($x=1;$x<=$sm.total;$x++){
				$sm.steps.append('<a href="#" data-posi="'+ ($x - 1) +'">'+$x+'</a>');
			}
			$sm.steps.find('a').on('click',function(){
				$sm.posi 	= parseInt($(this).attr('data-posi'));
				$sm.newPosition();
				return false;
			});

			$sm.arrows.find('a').on('click',function(){
				type 	= $(this).attr('data-type');
				$sm.preventLimits(type);
				return false;
			});

			$(window).scroll(function() {
				$sm.isScrolledIntoView();
			});
			$sm.applyClass();
			$sm.isScrolledIntoView();
		}
	}

	$sm.newPosition = function(){
		$('.LPELP_smoothiesList li.item').removeClass('active');
		$('.LPELP_smoothiesList li.item:eq('+$sm.posi+')').addClass('active');
		$sm.applyClass();
	}

	$sm.applyClass = function(){
		$sm.steps.find('a').removeClass('active');
		if($sm.posi == 0){
			$sm.arrows.find('a.prev').addClass('hidden');
		} else {
			$sm.arrows.find('a.prev').removeClass('hidden');
		}
		if($sm.posi == $sm.total - 1){
			$sm.arrows.find('a.next').addClass('hidden');
		} else {
			$sm.arrows.find('a.next').removeClass('hidden');	
		}
		$sm.steps.find('a:eq('+$sm.posi+')').addClass('active');
		if($sm.posi%2 == 1){
			$sm.obj.addClass('par');
		} else {
			$sm.obj.removeClass('par');	
		}
	}

	$sm.preventLimits = function(way){
		if(way == 'left'){
			$sm.posi++;
			$sm.posi = $sm.posi>$sm.total - 1 ? $sm.total - 1 : $sm.posi;
		} else if(way == 'right'){
			$sm.posi--;
			$sm.posi = $sm.posi<0 ? 0 : $sm.posi;
		}
		$sm.newPosition();
	}

	$sm.isScrolledIntoView 	= function(){
		var docViewTop 		= $(window).scrollTop();
		var docViewBottom 	= docViewTop + $(window).height();
		var elemTop 		= $sm.obj.offset().top;
		var elemBottom 		= elemTop + $sm.obj.height();
		if(docViewBottom <= elemBottom){
			$sm.obj.addClass('fixed');
		} else {
			$sm.obj.removeClass('fixed');
		}
	}

/* END SMOOTHIES NAVIGATION */

$o.createCarrusel = function(){
	$o.target 		= !$o.target ? $('.carrusel') : $o.target;
	if($o.target.length){
		$o.items = [];
		$o.target.each(function(){
			$p 			= {}
			$p.name 	= $(this).attr('id');
			$p.id 		= $('#' + $p.name);
			$p.active 	= false;
			$p.posi 	= 0; //Carousel default position
			$p.wrap 	= !$p.wrap ? $p.id.find('.wrapList') : $p.wrap;
			$p.step 	= $p.wrap.width();
			$p.nav 		= $p.id.find('nav.pagination');
			$p.list 	= $p.id.find('ul');
			$p.lItem 	= $p.id.find('li');
			$p.total 	= $p.lItem.length;
			if($p.nav.html() == ''){
				for($x=1;$x<=$p.total;$x++){
					$p.nav.append('<a href="#" data-posi="'+ ($x - 1) +'">'+$x+'</a>');
				}
				$p.nav.find('a').on('click',function(){
					posi 	= parseInt($(this).attr('data-posi'));
					parent 	= $(this).closest('.carrusel').attr('id');
					$o.changePosition(posi,parent);
					return false;
				});
			}

			if($p.nav.css('display') !== 'none') {
				$p.wrap.addClass('mobileView');
				$p.lItem.attr('style','width:' + $p.step + 'px');
				$p.list.attr('style','width:' + $p.step * $p.total + 'px');
			} else {
				$p.wrap.removeClass('mobileView');
				$p.lItem.attr('style','');
				$p.list.attr('style','');
			}

			$p.id.find('nav.steps a').off().on('click',function(event){
				type 	= $(this).attr('data-type');
				parent 	= $(this).closest('.carrusel').attr('id');
				$o.preventLimits(type,parent);
				return false;
			});

			$o.items[$p.name] = $p;
			$o.applyClass($p.name);
		})
	}
}

//Function to animate the carousel to the new position
$o.changePosition = function(posi,parent){
	$t 			= $o.items[parent];
	$t.posi 	= posi;
	move 		= $t.step * $t.posi * (-1);
	$t.list.css('left',move);
	$t.active = false;
	$o.applyClass(parent);
}

//Function to detect the arrow pagination direction and next position item
$o.preventLimits = function(way,parent){
	$t 			= $o.items[parent];
	if(way == 'left'){
		$t.posi++;
		$t.posi = $t.posi>$t.total - 1 ? $t.total - 1 : $t.posi;
	} else if(way == 'right'){
		$t.posi--;
		$t.posi = $t.posi<0 ? 0 : $t.posi;
	}
	$o.changePosition($t.posi,parent);
}

//function to change the status of navigation items when movement end
//Pagination arrows and bullet pagination
$o.applyClass = function(parent){
	$t 			= $o.items[parent];
	$t.nav.find('a').removeClass('active');
	if($t.posi == 0){
		$t.id.find('nav.steps a.prev').addClass('hidden');
	} else {
		$t.id.find('nav.steps a.prev').removeClass('hidden');
	}
	if($t.posi == $t.total - 1){
		$t.id.find('nav.steps a.next').addClass('hidden');
	} else {
		$t.id.find('nav.steps a.next').removeClass('hidden');	
	}
	$t.nav.find('a:eq('+$t.posi+')').addClass('active');
	if($t.total <=1){
		$t.nav.hide();
	}
}