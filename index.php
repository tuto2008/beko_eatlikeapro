<?php 
	$base_url 	= 'http://'.$_SERVER["SERVER_NAME"].'/';
	$path 		= $_SERVER["DOCUMENT_ROOT"];
	$pages 		= array('home','mission','recipes1','recipes2','recipes3','recipes4','recipes5','smoothies');
	$current 	= substr($_SERVER["REQUEST_URI"], 1);
	$v 			= '?v='.rand(1,999999); 

	$file 		= $path.'/home.html';
	$active 	= array('','','','','','','','');
	$subActive	= array('','','','','');
	$clave 		= 0;

	if($current && in_array($current, $pages) ){
		if( file_exists($path.'/'.$current.'.html')){
			$file 		= $path.'/'.$current.'.html';
			$active[0] 	= '';
			$clave 		= array_search($current, $pages);
		}
	}

	$ShareInfo = array(
		'By 2025, 70 million children around the world will suffer from obesity',
		'Every year, statistics show children are eating worse: less fresh vegetables, fruits and fish, and more fat and sugar',
		'Quick recovery healthy pizza to be as fast as Messi',
		'Muscle Builder Roast Beef to be strong like umtiti',
		'All-Powerful banana and avocado pudding to jump as high as Piqué',
		'Energy boost bars to be as powerful as Suárez',
		'Mind sharpener gazpacho with tuna to become as accurate as Rakitić',
		'The winners\' smoothies',
	);

	$ShareImage 	= $base_url.'assets/images/'.$current.'_image_share.jpg';

	$subActive[$clave - 2] = $clave>=2 && $clave<=6 ? ' class="active"' : '';
	$active[$clave] = ' active';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $ShareInfo[$clave]; ?></title>

	<!-- facebook share -->
	<meta property="og:url"					content="<?php echo $base_url.$current; ?>" />
	<meta property="og:type"				content="article" />
	<meta property="og:title"				content="RECIPES TO EAT LIKE A PRO" />
	<meta property="og:description"			content="<?php echo $ShareInfo[$clave]; ?> - BEKO" />
	<meta property="og:image"				content="<?php echo $ShareImage; ?>" />

	<!-- twitter share -->
	<meta name="twitter:card" 				content="summary_large_image">
	<meta name="twitter:site" 				content="@Beko">
	<meta name="twitter:creator" 			content="@Beko">
	<meta name="twitter:title" 				content="RECIPES TO EAT LIKE A PRO">
	<meta name="twitter:description" 		content="<?php echo $ShareInfo[$clave]; ?> - BEKO">
	<meta name="twitter:image"				content="<?php echo $ShareImage; ?>">

	<style>
		html, body, div, span, applet, object, iframe,h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big,cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike,strong, sub, sup, tt, var, dl, dt, dd, ol, ul, li, fieldset, form, label, legend,table, caption, tbody, tfoot, thead, tr, th, td, center, u, b, i{margin:0;padding:0;border:0;outline:0;font-weight:normal;font-style:normal;font-size:100%;font-family:inherit;vertical-align:baseline}body{line-height:1}:focus{outline:0}ol, ul{list-style:none}table{border-collapse:collapse;border-spacing:0}blockquote:before, blockquote:after, q:before, q:after{content:""}blockquote, q{quotes:"" ""}input, textarea{margin:0;padding:0}hr{margin:0;padding:0;border:0;color:#000;background-color:#000;height:1px}strong{font-weight:bold}
	</style>
	<link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/custom.css<?php echo $v; ?>">
	<link rel="stylesheet" href="<?php echo $base_url; ?>assets/css/fonts.css">
	<link rel="stylesheet" href="http://beko.ondevelope.com/assets/css/fixed.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<script src="https://code.jquery.com/jquery-1.8.3.min.js" integrity="sha256-YcbK69I5IXQftf/mYD8WY0/KmEDCv1asggHpJk1trM8="  crossorigin="anonymous"></script>
	<script src="<?php echo $base_url; ?>assets/js/main.js<?php echo $v; ?>"></script>
</head>
<body class="debuger">
	<!--<div class="ancho"></div>-->

	<!-- LPELP_MainContent -->
	<div class="LPELP_MainContent">
		<!-- LPELP_header -->
		<header class="LPELP_header">
			<div class="LPELP_wrap">
				<a href="./home" class="LPELP_logo"><img src="assets/images/logotipo.png"></a>
				<nav class="LPELP_mainNav">
					<ul id="LPELP_mainMenu">
						<li class="mainItem item1<?php echo $active[1]; ?>"><a href="./mission">Our mission</a></li>
						<li class="mainItem item2<?php echo $active[2].$active[3].$active[4].$active[5].$active[6]; ?>">
							<a class="nofollow" href="./recipes1">Recipes</a>
							<ul class="LPELP_submenu">
								<li class="subitem sub1"><a href="./recipes1"<?php echo $subActive[0]; ?>>Quick Recovery Healthy Pizza</a></li>
								<li class="subitem sub2"><a href="./recipes2"<?php echo $subActive[1]; ?>>Muscle builder roast-beef rolls</a></li>
								<li class="subitem sub3"><a href="./recipes3"<?php echo $subActive[2]; ?>>All-Powerful Banana and Avocado Pudding</a></li>
								<li class="subitem sub4"><a href="./recipes4"<?php echo $subActive[3]; ?>>Energy Boost Bars</a></li>
								<li class="subitem sub5"><a href="./recipes5"<?php echo $subActive[4]; ?>>Mind Sharpener Gazpacho with Tuna</a></li>
							</ul>
						</li>
						<li class="mainItem item3<?php echo $active[7]; ?>"><a href="./smoothies">Smoothies</a></li>
					</ul>
				</nav>
			</div>
		</header>
		<!-- END LPELP_header -->

		<?php 
			$content = str_replace("{base_url}",$base_url,file_get_contents($file));
			echo $content;
			//include($file); 
		?>

		<div id="LPELP_videoModal">
			<a href="#" class="LPELP_modalClose">Close</a>
			<div class="LPELP_videoPlayer"></div>
		</div>
	</div>
	<!-- END LPELP_MainContent -->

</body>
</html>